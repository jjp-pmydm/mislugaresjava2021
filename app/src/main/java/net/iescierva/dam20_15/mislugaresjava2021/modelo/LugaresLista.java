package net.iescierva.dam20_15.mislugaresjava2021.modelo;

import java.util.ArrayList;
import java.util.List;

public class LugaresLista implements net.iescierva.dam20_15.mislugaresjava2021.modelo.RepositorioLugares {
    protected List<net.iescierva.dam20_15.mislugaresjava2021.modelo.Lugar> listaLugares;

    public LugaresLista() {
        listaLugares = new ArrayList<>();
        addEjemplos();
    }

    public net.iescierva.dam20_15.mislugaresjava2021.modelo.Lugar get_element(int id) {
        return listaLugares.get(id);
    }

    public void add(net.iescierva.dam20_15.mislugaresjava2021.modelo.Lugar lugar) {
        listaLugares.add(lugar);
    }

    public int add_blank() {
        net.iescierva.dam20_15.mislugaresjava2021.modelo.Lugar lugar = new net.iescierva.dam20_15.mislugaresjava2021.modelo.Lugar();
        listaLugares.add(lugar);
        return listaLugares.size()-1;
    }

    public void delete(int id) {
        listaLugares.remove(id);
    }

    public int size() {
        return listaLugares.size();
    }

    public void update_element(int id, net.iescierva.dam20_15.mislugaresjava2021.modelo.Lugar lugar) {
        listaLugares.set(id, lugar);
    }

    public void addEjemplos() {
        add(new net.iescierva.dam20_15.mislugaresjava2021.modelo.Lugar("Escuela Politécnica Superior de Gandía",
                "C/ Paranimf, 1 46730 Gandia (SPAIN)",
                38.995656,-0.166093,
                net.iescierva.dam20_15.mislugaresjava2021.modelo.TipoLugar.EDUCACION, 962849300,
                "http://www.epsg.upv.es",
                "Uno de los mejores lugares para formarse.", 3));
        add(new net.iescierva.dam20_15.mislugaresjava2021.modelo.Lugar("Al de siempre",
                "P.Industrial Junto Molí Nou - 46722, Benifla (Valencia)",
                38.925857, -0.190642,
                net.iescierva.dam20_15.mislugaresjava2021.modelo.TipoLugar.BAR, 636472405,
                "",
                "No te pierdas el arroz en calabaza.", 3));
        add(new net.iescierva.dam20_15.mislugaresjava2021.modelo.Lugar("androidcurso.com", "ciberespacio",
                0.0, 0.0,
                net.iescierva.dam20_15.mislugaresjava2021.modelo.TipoLugar.EDUCACION, 962849300,
                "http://androidcurso.com",
                "Amplia tus conocimientos sobre Android.", 5));
        add(new net.iescierva.dam20_15.mislugaresjava2021.modelo.Lugar("Barranco del Infierno",
                "Vía Verde del río Serpis. Villalonga (Valencia)",
                38.867180,-0.295058,
                net.iescierva.dam20_15.mislugaresjava2021.modelo.TipoLugar.NATURALEZA, 0,
                "http://sosegaos.blogspot.com.es/2009/02/lorcha-villalonga-via-"+
                        "verde-del-rio.html","Espectacular ruta para bici o andar", 4));
        add(new net.iescierva.dam20_15.mislugaresjava2021.modelo.Lugar("La Vital",
                "Avda. de La Vital, 0 46701 Gandia (Valencia)",
                38.9705949, -0.1720092,
                net.iescierva.dam20_15.mislugaresjava2021.modelo.TipoLugar.COMPRAS, 962881070,
                "http://www.lavital.es/",
                "El típico centro comercial", 2));
    }
}