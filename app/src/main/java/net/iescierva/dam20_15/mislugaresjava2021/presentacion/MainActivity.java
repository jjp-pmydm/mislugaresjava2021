package net.iescierva.dam20_15.mislugaresjava2021.presentacion;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import net.iescierva.dam20_15.mislugaresjava2021.R;

public class MainActivity<AdaptadorLugares> extends AppCompatActivity {
    private RecyclerView recyclerView;
    public AdaptadorLugares adaptador;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.ItemDecoration separador;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //acceder a la vista "recycler_view" de la actividad actual (MainActivity)

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        //Construir el adaptador y asignarlo al recyclerView
        Object lugares = null;
        adaptador = new AdaptadorLugares(this, lugares);
        recyclerView.setAdapter((RecyclerView.Adapter) adaptador);

        //Construir y asignar el LayoutManager, en este caso de tipo Linear
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //Construir y asignar el separador de elementos
        separador = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(separador);
    }
}
