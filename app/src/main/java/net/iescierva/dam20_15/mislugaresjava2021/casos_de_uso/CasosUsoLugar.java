package net.iescierva.dam20_15.mislugaresjava2021.casos_de_uso;

import android.app.Activity;
import android.content.Intent;

import androidx.appcompat.app.AlertDialog;

import net.iescierva.dam20_15.mislugaresjava2021.R;
import net.iescierva.dam20_15.mislugaresjava2021.modelo.Lugar;
import net.iescierva.dam20_15.mislugaresjava2021.modelo.RepositorioLugares;
import net.iescierva.dam20_15.mislugaresjava2021.presentacion.EdicionLugarActivity;
import net.iescierva.dam20_15.mislugaresjava2021.presentacion.VistaLugarActivity;

public class CasosUsoLugar {
    private Activity actividad;
    private RepositorioLugares lugares;

    public CasosUsoLugar(Activity actividad, RepositorioLugares lugares) {
        this.actividad = actividad;
        this.lugares = lugares;
    }

    public void mostrar(int pos, Activity activity) {
        if (pos > lugares.size() - 1){
            new AlertDialog.Builder(activity)
                    .setTitle("\t\tError")
                    .setIcon(R.drawable.alert)
                    .setMessage("\n\nEse lugar no existe. Sólo hay "
                            + (lugares.size() - 1) + " lugares guardados.")
                    .setPositiveButton("Volver", null)
                    .show();
            return;
        }
        Intent i = new Intent(actividad, VistaLugarActivity.class);
        i.putExtra("pos", pos);
        actividad.startActivity(i);
    }

    public void guardar(int id, Lugar nuevoLugar) {
        lugares.update_element(id, nuevoLugar);
    }

    public void editar(int pos, int codigoSolicitud) {
        Intent i = new Intent(actividad, EdicionLugarActivity.class);
        i.putExtra("pos", pos);
        actividad.startActivityForResult(i, codigoSolicitud);
    }

    public void borrarLugar(final int id) {
        new AlertDialog.Builder(actividad)
                .setTitle("Borrado de lugar")
                .setMessage("¿Estás seguro de que quieres eliminar este lugar?")
                .setPositiveButton("Confirmar", (dialog, whichButton) -> {
                    lugares.delete(id);
                    actividad.finish();
                })
                .setNegativeButton("Cancelar", null)
                .show();
    }
}