package net.iescierva.dam20_15.mislugaresjava2021.presentacion;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import net.iescierva.dam20_15.mislugaresjava2021.modelo.Lugar;
import net.iescierva.dam20_15.mislugaresjava2021.R;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class GuardarLugarActivity extends AppCompatActivity {

    public static Map<Lugar, String> lugares= new HashMap<>();
    ImageView addPhoto;
    FloatingActionButton deletePhotoButton;
    TextView addPhotoText;
    boolean photoTaken = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edicion_lugar);

        addPhoto = (ImageView) findViewById(R.id.añadir_imagen);
        deletePhotoButton = (FloatingActionButton) findViewById(R.id.delete_photo_button);
        addPhotoText = (TextView) findViewById(R.id.añadirImagenText);

        addPhoto.setOnClickListener(view -> {
            if (view.equals(addPhoto) && !photoTaken){
                dispatchTakePictureIntent();
            }
        });
        deletePhotoButton.setOnClickListener(this::deleteImage);
    }

    static final int REQUEST_TAKE_PHOTO = 1;
    Uri currentPhotoURI;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                currentPhotoURI= FileProvider.getUriForFile(this,
                        "net.iescierva.ajfp.mislugaresjava2021.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, currentPhotoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                photoTaken = true;
            }
        }
    }

    /* Ya que sin complicar el código no podemos saber cuándo está disponible la fotografía
     * simplemente nos aseguramos de que se acceda a ella cuando la actividad vuelva a abrirse
     * tras cerrarse la cámara */
    @Override
    public void onResume() {
        super.onResume();
        if (photoTaken){
            addPhotoText.setText("Compruebe la imagen guardada.\n");
            addPhoto.setImageBitmap(BitmapFactory.decodeFile(currentPhotoPath));
            deletePhotoButton.setVisibility(View.VISIBLE);
        }
    }

    String currentPhotoPath;
    File lastImage;

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.FRANCE).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
       lastImage = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = lastImage.getAbsolutePath();
        return lastImage;
    }

    private void deleteImage(View view){
        lastImage.delete();
        deletePhotoButton.setVisibility(View.GONE);
        addPhotoText.setText(R.string.añada_image);
        addPhoto.setImageResource(R.drawable.add_photo);
        photoTaken = false;
    }
}