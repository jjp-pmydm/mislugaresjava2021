package net.iescierva.dam20_15.mislugaresjava2021.presentacion;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import net.iescierva.dam20_15.mislugaresjava2021.R;

public class PreferenciasFragment extends PreferenceFragment {
    @Override
    //Para resumir la actividad queda guardado el estado en forma de paquete
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferencias);
    }
}
