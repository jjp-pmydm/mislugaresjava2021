package net.iescierva.dam20_15.mislugaresjava2021.presentacion;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class PreferenciasActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //El FragmentManager utiliza una transacción para introducir y ordenar los fragmentos
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new net.iescierva.dam20_15.mislugaresjava2021.presentacion.PreferenciasFragment())
                //.replace(new Fragment())
                .commit();
    }
}
