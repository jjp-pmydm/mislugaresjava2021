package net.iescierva.dam20_15.mislugaresjava2021.modelo;

public interface RepositorioLugares {
    net.iescierva.dam20_15.mislugaresjava2021.modelo.Lugar get_element(int id); //Devuelve el elemento dado su id
    void add(net.iescierva.dam20_15.mislugaresjava2021.modelo.Lugar lugar); //Añade el elemento indicado
    int add_blank(); //Añade un elemento en blanco y devuelve su id
    void delete(int id); //Elimina el elemento con el id indicado
    int size(); //Devuelve el número de elementos
    void update_element(int id, net.iescierva.dam20_15.mislugaresjava2021.modelo.Lugar lugar); //Reemplaza un elemento
}
