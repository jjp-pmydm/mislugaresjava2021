package net.iescierva.dam20_15.mislugaresjava2021.presentacion;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import net.iescierva.dam20_15.mislugaresjava2021.R;
import net.iescierva.dam20_15.mislugaresjava2021.casos_de_uso.CasosUsoLugar;
import net.iescierva.dam20_15.mislugaresjava2021.datos.CustomApplication;
import net.iescierva.dam20_15.mislugaresjava2021.modelo.Lugar;
import net.iescierva.dam20_15.mislugaresjava2021.modelo.RepositorioLugares;

import java.text.DateFormat;
import java.util.Date;

public class VistaLugarActivity extends AppCompatActivity {

    private RepositorioLugares lugares;
    private CasosUsoLugar usosLugar;
    private int pos;
    private Lugar lugar;
    final static int RESULTADO_EDITAR = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vista_lugar);
        Bundle extras = getIntent().getExtras();
        pos = extras.getInt("pos", 0);
        lugares = ((CustomApplication) getApplication()).getLugares();
        usosLugar = new CasosUsoLugar(this, lugares);
        lugar = lugares.get_element(pos);
        actualizaVistas();
    }

    private void checkInfoDisponible() {
        LinearLayout layoutTelef = findViewById(R.id.layoutTelef);
        LinearLayout layoutComent = findViewById(R.id.layoutComent);
        LinearLayout layoutURL = findViewById(R.id.layoutURL);
        LinearLayout layoutDirecc = findViewById(R.id.layoutDirecc);

        if (lugar.getTelefono() == 0) {
            layoutTelef.setVisibility(View.GONE);
        } else {
            layoutTelef.setVisibility(View.VISIBLE);
            TextView vistaTelef = findViewById(R.id.telefonoVista);
            vistaTelef.setText(Integer.toString(lugar.getTelefono()));
        }

        if (lugar.getComentario().equals("")) {
            layoutComent.setVisibility(View.GONE);
        } else {
            layoutComent.setVisibility(View.VISIBLE);
            TextView vistaComent = findViewById(R.id.comentarioVista);
            vistaComent.setText(lugar.getComentario());
        }

        if (lugar.getUrl().equals("")) {
            layoutURL.setVisibility(View.GONE);
        } else {
            layoutURL.setVisibility(View.VISIBLE);
            TextView vistaURL = findViewById(R.id.urlVista);
            vistaURL.setText(lugar.getUrl());
        }

        if (lugar.getDireccion().equals("")) {
            layoutDirecc.setVisibility(View.GONE);
        } else {
            layoutDirecc.setVisibility(View.VISIBLE);
            TextView vistaDirecc = findViewById(R.id.direccionVista);
            vistaDirecc.setText(lugar.getDireccion());
        }
    }

    public void actualizaVistas() {
        TextView nombre = findViewById(R.id.nombreLugar);
        ImageView logo_tipo = findViewById(R.id.logo_tipo);
        TextView tipo = findViewById(R.id.tipo);
        TextView fecha = findViewById(R.id.fecha);
        TextView hora = findViewById(R.id.hora);
        RatingBar valoracion = findViewById(R.id.valoracion);

        nombre.setText(lugar.getNombre());
        logo_tipo.setImageResource(lugar.getTipo().getRecurso());
        tipo.setText(lugar.getTipo().getTexto());
        fecha.setText(DateFormat.getDateInstance().format(
                new Date(lugar.getFecha())));
        hora.setText(DateFormat.getTimeInstance().format(
                new Date(lugar.getFecha())));
        valoracion.setRating(lugar.getValoracion());
        valoracion.setOnRatingBarChangeListener(
                (ratingBar, valor, fromUser) -> lugar.setValoracion(valor));
        checkInfoDisponible();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.vista_lugar_menu, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULTADO_EDITAR) {
            actualizaVistas();
            findViewById(R.id.scrollView1).invalidate();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accion_compartir:
                return true;
            case R.id.accion_llegar:
                return true;
            case R.id.accion_editar:
                usosLugar.editar(pos, 1);
                return true;
            case R.id.accion_borrar:
                usosLugar.borrarLugar(pos);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}